const FIRST_NAME = "Adina-Camelia";
const LAST_NAME = "Cratau";
const GRUPA = "1090";

/**
 * Make the implementation here
 */

function numberParser(value) {
    if(value !== String || value === Infinity || value === -Infinity)
        return parseInt(value);
    if(value < Number.MIN_SAFE_INTEGER)
        return 5;
    if(value > Number.MAX_SAFE_INTEGER)
        return NaN;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}
